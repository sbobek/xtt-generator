# XTT Generator v.4.0 #
Zrobić na wiki opis działaniam uruchamiania itd.

The current version of the application is a final XTT generator.

### How to use that? ###

The application is launched from command line with 2 arguments:

1. the name of the config file (input)
2. the name of the output file containing the generated *XTT* model


```
#!console

> java -jar XTTGen.jar config_file.txt output_file.txt
```


### Other information ###
* [JavaDoc documentation](http://myownsite.cba.pl/xtt/)