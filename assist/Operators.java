package assist;
/**
 * Class containing all operators used in schemas and rules definitions (as String arrays and
 * <code>public static final</code> fields). It consists of String arrays:
 * <ul>
 *  <li>SIMPLE_SIMPLE array contains operators used when simple attributes are on the left and right side of the operator,</li>
 *  <li>SIMPLE_GENERAL array contains operators used when simple attributes are on the left and general on
 * the right side of the operator, </li>
 *  <li>GENERAL_GENERAL array contains operators used when general attributes are on the left and right side of the operator.</li>
 * </ul>
 * There are also final String fields, more convenient for using by human. Arrays are easier to use while random creating
 * the schemas and rules.
 * @author Piotr Misiak
 */
public class Operators {
    /**
     * Array of operators used with simple attributes and simple values.
     * 0th element is "eq", 1st element is "neq".
     */
    public static final String [] SIMPLE_SIMPLE = {"eq", "neq"};
    /**
     * Array of operators used with simple attributes and set/list values.
     * 0th element is "in", 1st element is "notin".
     */
    public static final String [] SIMPLE_GENERAL = {"in", "notin"};
    /**
     * Array of operators used with general attributes and set/list values.
     * 0th element is "supset", 1st element is "subset", 2nd element is "sim", 3rd element is "notsim",
     * 4th element is "eq", 5th element is "noteq".
     */
    public static final String [] GENERAL_GENERAL = {"supset",
        "subset","sim","notsim","eq","noteq"};

    /**
     * The 'eq' (equal) operator.
     */
    public static final String EQ = "eq";

    /**
     *The 'neq' (not equal) operator.
     */
    public static final String NEQ = "neq";

    /**
     * The 'in' operator.
     */
    public static final String IN = "in";

    /**
     * The 'notin' operator.
     */
    public static final String NOTIN = "notin";

    /**
     * The 'subset' operator.
     */
    public static final String SUBSET = "subset";

    /**
     * The 'supset' (superset) operator.
     */
    public static final String SUPSET = "supset";

    /**
     * The 'sim' (similar) operator.
     */
    public static final String SIM = "sim";

    /**
     * The 'notsim' (not similar) operator.
     */
    public static final String NOTSIM = "notsim";
    /**
     * Special 'set' operator used inside a decisions (right-side expression in a rule).
     */
    public static final String SET = "set";
}
