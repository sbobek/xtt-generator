/**
*     Copyright 2013-15 by Szymon Bobek, Grzegorz J. Nalepa, Mateusz Ślażyński
*
*     This file is part of HeaRTDroid.
*     HeaRTDroid is a rule engine that is based on HeaRT inference engine,
*     XTT2 representation and other concepts developed within the HeKatE project .
*
*     HeaRTDroid is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     HeaRTDroid is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with HeaRTDroid.  If not, see <http://www.gnu.org/licenses/>.
*
**/
package assist;
import java.util.SortedSet;
import xttgen.Attribute;
/**
 * Class representing simple formulae in HMR (attribute+operator+value/set).
 * (the author of the original version: sbk)
 * @author Piotr Misiak
 * @version 1.1
 */
public class Formulae {
    public Formulae() {}
    /**
     * Field containing the attribute in the formulae.
     */
    private Attribute attribute;

    /**
     * Field containing the operator in the formulae.
     */
    private String operator;

    /**
     * Field containing the single value in the formulae (for eq and neq operators).
     */
    private NumberedString simpleValue;

    /**
     * Field containing the set value or the set of satisfing simple values.
     */
    private SortedSet<NumberedString> setValue;
    
    public Attribute getAttribute() {
        return attribute;
    }
    
    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }
    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public NumberedString getSimpleValue() {
        return simpleValue;
    }

    public void setSimpleValue(NumberedString simpleValue) {
        this.simpleValue = simpleValue;
    }

    public SortedSet<NumberedString> getSetValue() {
        return setValue;
    }

    public void setSetValue(SortedSet<NumberedString> setValue) {
        this.setValue = setValue;
    }
    
    /**
     * Overriden toString() method returns the text: attribute name, operator, value (simple value or set value - 
     * the field that is not null is appended).
     * @return string representaion of the formulae
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\r\n").append(attribute.getName());
        sb.append(" ").append(operator).append(" ");
        if (setValue != null)
            sb.append(setValue);
        else sb.append(simpleValue);
        sb.append(" ");
        return sb.toString();
    }
}
