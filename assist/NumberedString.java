package assist;

import com.sun.istack.internal.NotNull;

import java.util.DoubleSummaryStatistics;

/**
 * Class representing one item of the domain. It consists of four fields which are used according
 * to the category of the domain (numeric, symbolic, explicit, range domain). The class contains four
 * overloaded parametric constructors for different types of the domain.
 * @author Piotr Misiak
 * @version 1.0
 */
public class NumberedString implements Comparable{
    /**
     * Field containing the numeric value (for numeric types) or weight of the value (for symbolic types)
     */
    protected Double doubleField;
    /**
     * Field containing character/string value (for symbolic types)
     */
    protected String stringField;
    /**
     * Field containing the start value of the range domain
     */
    protected Double startDouble;
    /**
     *Field containing the end value of the range domain
     */
    protected Double endDouble;
    /**
     * Parametric constructor used for numeric range domain.
     * @param start beginning of the range
     * @param end end of the range
     */
    public NumberedString (double start, double end) {
        startDouble = start;
        endDouble = end;
    }

    /**
     * Parametric constructor used for numeric explicit domain.
     * @param value value that is to be assigned to the field of the domain element
     */
    public NumberedString (double value) {
        doubleField = value;
    }
    
    /**
     * Parametric constructor used for symbolic unordered domain
     * @param value value that is to be assigned to the field of the domain element
     */
    public NumberedString (String value) {
        stringField = value;
    }
    
    /**
     * Parametric constructor used for symbolic ordered domain
     * @param value value that is to be assigned to the field of the domain element
     * @param weight weight of the element of the domain
     */
    public NumberedString (String value, int weight) {
        stringField = value;
        doubleField = (double) weight;
    }
    /**
     * Method comparing two NumberedString objects. The method uses intField value to compare
     * these objects. In case of symbolic unordered domain intField is null, so the stringFields are used to compare
     * two objects. In such a situation these two string are compared in a natural way.
     * @param t another object to compare with
     * @return -1 if value of this object is less than t's value, 1 if value of this object is greater than t's value,
     * 0 otherwise. 
     * For unordered symbolic domain, method returns 0 if both strings are equal, 1 otherwise;
     */
    @Override
    public int compareTo(@NotNull Object t) {
        NumberedString other = null;
        if (t instanceof NumberedString)  //if not, return ClassCastException
            other = (NumberedString) t;
        if (other == null) throw new ClassCastException();
        if (doubleField != null && other.doubleField != null) {   //for symbolic ordered types and numeric types
            if (this.doubleField < other.doubleField) return -1;
            if (this.doubleField > other.doubleField) return 1;
            return 0;
        }
        //for symbolic unordered domains:
        if (other.stringField != null) {
            if (this.stringField.length() < other.stringField.length()) return -1; //shorter string should be earlier that longer one
            if (this.stringField.length() > other.stringField.length()) return 1;
            return this.stringField.compareTo(other.stringField); //strings of the same length are sorted in alphabetical order
        }
        return 0;
    }
    
    /**
     * Overriden function returning string representation of one NumberedString object. For numeric explicit domain
     * it simply returns the number, for numeric range domain it returns both start and end numbers
     * separated by "to" word, e.g "1 to 10". For symbolic ordered domain it returns the symbol(s) (as string) and
     * its weight separated by slash (/) sign, and for symbolic unordered domain it returns only the symbol(s).
     * @return string representation of one element. In case of any errors about object's field content, it returns 
     * string "null".
     */
    @Override
    public String toString() {
        StringBuilder toReturn = new StringBuilder();
        if (doubleField != null && stringField == null) { //numeric base
            return Double.toString(doubleField.doubleValue());
        } 
        if (doubleField == null && stringField != null) { //symbolic unordered base
            return "'"+stringField+"'";
        }
        if (doubleField != null /* && stringField != null*/) { //symbolic ordered base
            return "'"+stringField+"'"+"/"+doubleField.intValue();

        }
        if (startDouble != null && endDouble != null) { //numeric base, domain as [from to to]
            return startDouble+" to "+endDouble;
        }
        return "null";
    }
    /**
     * Method returning the value of the string field of the attribute value
     * @return string value (string or single character) of the NumberedString object
     */
    public String getStringField() {
        return stringField;
    }
    /**
     * Method returning the value of the numeric field of the attribute value
     * @return numeric value of the NumberedString object
     */
    public double getDoubleField() {
        return doubleField;
    }
    /**
     * Method returning the start value of the attribute's range domain
     * @return string the start value of the attribute's range domain
     */
    public double getStartDouble() {
        return startDouble;
    }
    /**
     * Method returning the end value of the attribute's range domain
     * @return string the end value of the attribute's range domain
     */
    public double getEndDouble() {
        return endDouble;
    }
}
