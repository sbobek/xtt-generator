package xttgen;

import assist.CartesianIndicator;
import assist.Formulae;
import assist.NumberedString;
import assist.Operators;
import exceptions.BuilderException;
import exceptions.NotInTheDomainException;

import java.util.*;
import java.io.*;
import java.text.DecimalFormat;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class responsible for generating and storing single XTT set (one list of types, attributes with their states, schemas and rules).
 * This class is used for creating models for benchmark tests. Parameters of types are syntactically correct, but they
 * don't necessarily have any useful meaning in logical aspect. Methods creating the lists of types, attributes,
 * schemas and rules use the randomized algorithms.
 *
 * @author Piotr Misiak
 * @version 1.0
 */
public class Generator {
    /**
     * List of types in the current model.
     */
    private LinkedList<Type> types;
    /**
     * List of attributes  in the current model.
     */
    private LinkedList<Attribute> attributes;
    /**
     * List of schemas in the current model.
     */
    private LinkedList<Schema> schemas;
    /**
     * List of rules in the current model.
     */
    private LinkedList<Rule> rules;

    /**
     * Object containing attributes from the first schema and start values assigned to them
     */
    private Map<String,String> states;

    /**
     * object used for generating random values in used methods. It's mutual for all methods.
     */

    private Random rand = new Random();

    /**
     * Method saving the model (list of types in the generator) to HMR file in accordance with the definition
     * of HMR language. This method uses the toString() method of every Type object.
     *
     * @param outputStream BufferedWriter object indicating the destination HMR file
     */
    public void saveModel(BufferedWriter outputStream) throws IOException {
        if (types == null) {
            System.out.println("There is no model in the memory. You need   to create a model "
                    + "or load it from the file.");
            return;
        }
        System.out.println("> Saving model to the file ...");

        for (Type type : types)
            outputStream.write(type.toString());
        System.out.println("Model was successfully saved to the file.");
    }

    /**
     * Method saving the list of attributes in the generator to the HMR file in accordance with the definition
     * of HMR language. This method uses the toString() method of every Attribute object.
     *
     * @param outputStream BufferedWriter object indicating the destination HMR file
     */
    public void saveAttributes(BufferedWriter outputStream) throws IOException {
        if (attributes == null) {
            System.out.println("There are no attributes in the memory. You need to create a list of attributes "
                    + "or load it from the file.");
            return;
        }
        System.out.println("> Saving attributes to the file ...");
        for (Attribute attr : attributes)
            outputStream.write(attr.toCompleteString());
        System.out.println("List of attributes was successfully saved to the file.");
    }

    /**
     * Method saving the list of schemas in the generator to the HMR file in accordance with the definition
     * of HMR language. This method uses the toString() method of every Schema object.
     *
     * @param outputStream BufferedWriter object indicating the destination HMR file
     */
    public void saveSchemas(BufferedWriter outputStream) throws IOException {
        if (schemas == null) {
            System.out.println("There are no schemas in the memory. You need to create a list of schemas "
                    + "or load it from the file.");
            return;
        }
        System.out.println("> Saving schemas to the file ...");
        for (Schema schema : schemas)
            outputStream.write(schema.toString());
        System.out.println("List of schemas was successfully saved to the file.");
    }

    /**
     * Method saving the list of rules in the generator to the HMR file in accordance with the definition
     * of HMR language. This method uses the toString() method of every Rule object.
     *
     * @param outputStream BufferedWriter object indicating the destination HMR file
     */
    public void saveRules(BufferedWriter outputStream) throws IOException {
        if (rules == null) {
            System.out.println("There are no rules in the memory. You need to create a list of rules "
                    + "or load it from the file.");
            return;
        }
        System.out.println("> Saving rules to the file ...");

        for (Rule rule : rules)
            outputStream.write(rule.toString());
        System.out.println("The list of rules was successfully saved to the file.");
    }

    public void saveStates (BufferedWriter outputStream) throws IOException {
        if (states == null) {
            System.out.println("There are no states in the memory.");
            return;
        }
        System.out.println("> Saving states to the file ...");
        outputStream.newLine();
        for (String k : states.keySet()) {
            outputStream.write("xstat input/1: ["+k+","+states.get(k)+"].\n");
        }
    }

    /**
     * Method creating list of types. It creates approximated number of numeric and symbolic types basing on
     * typesBases parameter. This parameter has to be the form of string <code>(a/b)</code>,
     * e.g <code>(2/1)</code> means there are 2 numeric types per 1 symbolic type. Domains are created in
     * pseudo-random way, but they meet the requirements given by the parameters.
     * Attention! In a situation when size of the domain is larger than 26, every symbolic types have the same domain
     * and this domain contains non-literal symbols (it's caused by the sequence of characters in Unicode), but
     * numeric domains are still created correctly.
     *
     * @param typesAmount the number of types which are to be created
     * @param typesRatio  the relation between amount of numeric and symbolic types
     * @param domainSize  predicted size of every domain
     * @param isOrdered   string indicating whether domain is ordered. It may have value "yes" or "no"
     * @param step        step in the range of numeric type (difference between closest elements)
     */
    public void createModel(int typesAmount, String typesRatio, int domainSize, boolean isOrdered, double step) {
        if (types != null) {
            Console c = System.console();
            if (c == null) {
                System.err.println("Error: No console!");
                return;
            }
            char ans = c.readLine("There is already some model in the memory. Do you want to overwrite? (y/n): ").charAt(0);
            if (ans == 'n')
                return;
        }
        types = new LinkedList<>();
        System.out.println("> Creating model...");
        DecimalFormat df = new DecimalFormat("##.#####");
        double eps = 1e-5;
        int numberedCount, symbolicCount;
        numberedCount = Integer.parseInt(typesRatio.substring(1, typesRatio.indexOf("/")));
        symbolicCount = Integer.parseInt(typesRatio.substring(typesRatio.indexOf("/") + 1, typesRatio.length() - 1));
        numberedCount = typesAmount / (numberedCount + symbolicCount) * numberedCount;
        Type.Builder builder = new Type.Builder();
        for (int i = 0; i < typesAmount; i++) {  //single iteration means single type
            SortedSet<NumberedString> tempDomain = new TreeSet<>();
            SortedSet<NumberedString> tempNoWeightDomain = new TreeSet<>();
            builder.setName("xtype_" + i);
            if (numberedCount > 0) {   //creating numeric-base type
                builder.setBase(Type.BASE_NUMERIC);
                --numberedCount;
                builder.setStep(step);
                boolean isRange = rand.nextBoolean();
                int randStart = rand.nextInt(50);
                double formattedDouble;
                if (isRange) {  //setting the range domain (1 element in the domain)
                    builder.setIsRangeDomain(true);
                    formattedDouble = Double.parseDouble(df.format(randStart + (domainSize - 1) * step));
                    tempDomain.add(new NumberedString(randStart, formattedDouble));
                    //setting explicit noWeightDomain (not printed to the HMR file)
                    //useful while creating rules
                    for (double j = randStart; j <= (randStart + (domainSize - 1) * step) + eps; j += step) {
                        formattedDouble = Double.parseDouble(df.format(j));
                        tempNoWeightDomain.add(new NumberedString(formattedDouble));
                    }
                } else {  //setting explicit domain
                    for (double j = randStart; j <= (randStart + (domainSize - 1) * step) + eps; j += step) {
                        formattedDouble = Double.parseDouble(df.format(j));
                        tempDomain.add(new NumberedString(formattedDouble));
                    }
                }
            } else {          //creating symbolic-base type
                builder.setBase(Type.BASE_SYMBOLIC);
                builder.setOrdered(isOrdered ? "yes" : "no");
                //char randStartChar = (char) (97 + rand.nextInt(26));  //26 = 122-97+1 'a'=97, 'z'=122
                //int randStart = 97 + rand.nextInt((122 - domainSize - 97 + 1 > 0 ? 122 - domainSize - 97 + 1 : 1));
                //int randStart = 97 + rand.nextInt(122 - 97 + 1);
                int randStart = rand.nextInt(27); // 0 corresponds to 'a', 26 corresponds to 'z'
                //Character c;
                if (isOrdered) {
                    for (int j = 0; j < domainSize; j++) {
                        //c = Character.toChars(randStart + j - 1)[0];
                        //tempDomain.add(new NumberedString(c.toString(), j));
                        //tempNoWeightDomain.add(new NumberedString(c.toString())); //adding the value without weight
                        tempDomain.add(new NumberedString(getStringFromNumber(randStart + j),j+1));
                        tempNoWeightDomain.add(new NumberedString(getStringFromNumber(randStart + j)));
                    }
                } else {
                    for (int j = 0; j < domainSize; j++) {
                       // c = Character.toChars(randStart + j)[0];
                        //tempDomain.add(new NumberedString(c.toString()));
                        tempDomain.add(new NumberedString(getStringFromNumber(randStart + j)));
                    }
                }
            }
            builder.setDomain(tempDomain);
            builder.setNoWeightDomain(tempNoWeightDomain);
            try {
                types.add(builder.build());
            } catch (BuilderException ex) {
                System.out.println(ex.getMessage());
            }
        }
        System.out.println("Model was successfully created.");
        System.gc();
    }

    /**
     * Method creating list of attributes of existing types.
     * @param attributesAmount demanded number of created attributes
     */
    public void createAttributes(int attributesAmount) {
        if (types == null) {
            System.out.println("There is no model in the memory. You need to create a model "
                    + "or load it from the file.");
            return;
        }
        if (attributes != null) {
            Console c = System.console();
            if (c == null) {
                System.err.println("Error: No console!");
                return;
            }
            char ans = c.readLine("There is already some model in the memory. Do you want to overwrite? (y/n): ").charAt(0);
            if (ans != 'y')
                return;
        }
        attributes = new LinkedList<>();
        System.out.println("> Creating attributes list...");
        boolean isSimple;
        Attribute.Builder builder = new Attribute.Builder();
        for (int i = 0; i < attributesAmount; i++) {
            try {
                builder.setName("xattr_" + i);
                builder.setAbbreviation("xattr_"+i);
//                isSimple = rand.nextBoolean();
//                if (isSimple)
                    builder.setXTTClass(Attribute.CLASS_SIMPLE);
//                else
//                    builder.setXTTClass(Attribute.CLASS_GENERAL);
                builder.setType(types.get(rand.nextInt(types.size())));
                attributes.add(builder.build());
            } catch (BuilderException | NotInTheDomainException ex) {
                System.out.println(ex.getMessage());
            }
        }
        System.out.println("List of attributes was successfully created.");
        System.gc();
    }

    /**
     * Method creating list of schemas. Amount of created schemas may be lower than given argument since all attributes may
     * be used before all demanded schemas are created.
     * @param schemasAmount amount of schemas which are to be created
     * @param minAttrsAmount minimal amount of attributes which will exist in LHS or RHS of the schema
     * @param maxAttrsAmount maximal amount of attributes which will exist in LHS or RHS of the schema
     */
    public void createSchemas(int schemasAmount, int minAttrsAmount, int maxAttrsAmount) {
        if (schemasAmount < 1) {
            System.out.println("Error! Schemas amount is lower than 1.");
            return;
        }
        if (minAttrsAmount > maxAttrsAmount) {
            System.out.println("Error! The lower bound of attributes amount is greater than the upper bound.");
            return;
        }

        if (attributes == null) {
            System.out.println("There are no attributes in the memory. You need to create the list first "
                    + "or load it from the file.");
            return;
        }
        if (schemas != null) {
            Console c = System.console();
            if (c == null) {
                System.err.println("Error: No console!");
                return;
            }
            char ans = c.readLine("There is already some list of schemas in the memory. Do you want to overwrite? (y/n): ").charAt(0);
            if (ans != 'y')
                return;
        }
        schemas = new LinkedList<>();
        System.out.println("> Creating the list of schemas ...");
        Schema.Builder builder = new Schema.Builder();
        LinkedList<Attribute> tempAttrsList = new LinkedList<>();
        int attrIndex = 0;
        int attrCount = minAttrsAmount + rand.nextInt(maxAttrsAmount-minAttrsAmount+1);
        for (int i=0; i<schemasAmount; i++) {
            builder.setName("xschm_"+i);
            try {
                for (int j=attrIndex; j<attrIndex+attrCount; j++) {
                    tempAttrsList.add(attributes.get(j));
                }
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Error while adding input attributes! Not enough attributes to create the next schema.");
                System.out.println(i+" schemas were created.");
                break;
            }
            builder.setInputAttrs((LinkedList<Attribute>) tempAttrsList.clone());
            tempAttrsList.clear();
            attrIndex = attrIndex + attrCount;
            attrCount = minAttrsAmount + rand.nextInt(maxAttrsAmount-minAttrsAmount+1);
            try {
                for (int j=attrIndex; j<attrIndex+attrCount; j++) {
                    tempAttrsList.add(attributes.get(j));
                }
            } catch (IndexOutOfBoundsException e) {
                if (tempAttrsList.isEmpty()) {
                    System.out.println("Error while adding output attributes! Not enough attributes to create the next schema.");
                    System.out.println(i+" schemas were created.");
                    break;
                }
            }

            builder.setOutputAttrs((LinkedList<Attribute>) tempAttrsList.clone());
            tempAttrsList.clear();
            try {
                    schemas.add(builder.build());
                } catch (BuilderException | NotInTheDomainException ex) {
                    System.out.println(ex.getMessage());
                }
        }
        System.out.println("The list of schemas was successfully created.");
    }

    /**
     * Method creating list of rules based on the existing list of schemas. Number of rules created per one schema is not
     * well defined (the minRulesPerSchema argument is only lower bound).
     * The decision statements are set in a random way, but their sizes for general attributes are defined by the
     * arguments (as percent of the entire domain of the attribute which is to be concerned). This size (so called
     * randomSize) is between sizeLowerBound and sizeUpperBound (both inclusive).
     * The arguments should be integer numbers. The following operators are exploited while creating the rules:
     * <ul>
     * <li> 'in' and 'eq' for simple attributes </li>
     * <li> 'sim' and 'eq' for general attributes </li>
     * </ul>
     *
     * @param sizeLowerBound the minimal size of satisfying set of values (percent value between 0 and 100)
     * @param sizeUpperBound the maximal size of satisfying set of values (percent value between 0 and 100)
     * @param minRulesPerSchema the minimal amount of rules which are supposed to describe one schema
     */
    public void createRules(int sizeLowerBound, int sizeUpperBound, int minRulesPerSchema) {
        if (sizeLowerBound > sizeUpperBound) {
            System.out.println("Error! The lower bound is greater than the upper bound!");
            return;
        }
        if (minRulesPerSchema < 1) {
            System.out.println("Error! The amount of rules is less than 1!");
            return;
        }
        if (schemas == null) {
            System.out.println("There are no schemas in the memory. You need to create the list first "
                    + "or load it from the file.");
            return;
        }
        if (rules != null) {
            Console c = System.console();
            if (c == null) {
                System.err.println("Error: No console!");
                return;
            }
            char ans = c.readLine("There is already some list of rules in the memory. Do you want to overwrite? (y/n): ").charAt(0);
            if (ans != 'y')
                return;
        }
        System.out.println("> Creating the list of rules...");
        rules = new LinkedList<>();
        int domainSize;
        Formulae tempFormulae;
        LinkedList<Formulae> tempFormulaes = new LinkedList<>();
        SortedSet<NumberedString> decisionDomain;
        Rule.Builder builder = new Rule.Builder();
        for (Schema schema : schemas) { //for each schema in schemas list
            List<List<SortedSet<NumberedString>>> subdomainsList = new LinkedList<>();
            //subdomainsList is the list of lists of subdomains; single element of subdomainsList is a list of disjoint subdomains
            for (Attribute attr : schema.getInputAttrs()) {
                subdomainsList.add(makeSubdomains(attr, minRulesPerSchema, schema.getInputAttrs().size()));
            }
            CartesianIndicator cartesianIndicator = new CartesianIndicator(subdomainsList);
            System.out.println("AMOUNT OF RULES FOR SCHEMA "+schema.getName()+": "+cartesianIndicator.getProductsLimit());
            for (int i=0; i<cartesianIndicator.getProductsLimit(); i++) { //for each new rule
                builder.setName(Integer.toString(i)); //name in the output file contains schema name
                builder.setSchema(schema);
                tempFormulaes.clear();
                for (int j=0; j<schema.getInputAttrs().size(); j++) {
                    tempFormulae = new Formulae();
                    //System.gc();
                    Attribute inAttr = schema.getInputAttrs().get(j);
                    tempFormulae.setAttribute(inAttr);
                    SortedSet<NumberedString> currentSubdomain = subdomainsList.get(j).get(cartesianIndicator.tab[j]);
                    if (inAttr.getXTTClass().equals(Attribute.CLASS_SIMPLE)) { //if the current attribute is simple
                        if (currentSubdomain.size() == 1) { //if the subdomain contains only one element
                            tempFormulae.setSimpleValue(currentSubdomain.first());
                            tempFormulae.setOperator(Operators.EQ);
                        }
                        else { //if the subdomain has more elements
                            tempFormulae.setSetValue(currentSubdomain);
                            tempFormulae.setOperator(Operators.IN);
                        }
                    } else { //if the current attribute is general
                        tempFormulae.setSetValue(currentSubdomain);
                        if (currentSubdomain.size() == 1) { //if the subdomain contains only one element
                            tempFormulae.setOperator(Operators.SIM);
                        } else {
                            tempFormulae.setOperator(Operators.EQ);
                        }
                    }
                    tempFormulaes.add(tempFormulae);
                }
                builder.setConditions((LinkedList<Formulae>) tempFormulaes.clone());
                cartesianIndicator.increment();

                /*
                Processing the decision attributes in the current rule - the following 'for' loop
                */
                tempFormulaes.clear();
                for (Attribute outAttr : schema.outputAttrs) { //for each out-attribute
                    tempFormulae = new Formulae();
                    tempFormulae.setAttribute(outAttr);
                    tempFormulae.setOperator(Operators.SET);
                    Object[] domainArray = outAttr.getType().getNoWeightDomain().toArray();
                    if (outAttr.getXTTClass().equals(Attribute.CLASS_SIMPLE)) {
                        //if the attribute is simple
                        tempFormulae.setSimpleValue((NumberedString) domainArray[rand.nextInt(domainArray.length)]);
                    } else {  //if the attribute is general
                        domainSize = outAttr.getType().getNoWeightDomain().size();
                        int randomSize = sizeLowerBound + rand.nextInt(sizeUpperBound - sizeLowerBound + 1);
                        int decisionDomainSize = domainSize * randomSize / 100;  //the size of the domain in the decision
                        decisionDomain = new TreeSet<>();
                        do {
                            decisionDomain.add((NumberedString) domainArray[rand.nextInt(domainArray.length)]);
                        }
                        while (decisionDomain.size() < decisionDomainSize);
                        tempFormulae.setSetValue(decisionDomain);
                    }
                    tempFormulaes.add(tempFormulae);
                }
                builder.setResults((LinkedList<Formulae>) tempFormulaes.clone());
                try {
                    rules.add(builder.build());
                } catch (BuilderException ex) {
                    Logger.getLogger(Generator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println("The list of rules was successfully created.");
    }

    public void createStates() {
        states = new HashMap<>();
        Schema initSchema = schemas.get(0);
        List<Attribute> initAttrs = initSchema.getInputAttrs();
        for (Attribute a : initAttrs) {
            NumberedString [] domain = a.getType().getNoWeightDomain().toArray(new NumberedString[0]);
            NumberedString initValue = domain[rand.nextInt(domain.length)];
            states.put(a.getName(),initValue.toString());
        }
    }

    /**
     * Private method generating string (with length of one or two or more) with content depending on the given number. For numbers
     * between 0 and 25 it returns single letter ('a' to 'z'). For greater numbers it returns two-letter string: 'aa' for 26, 'ab' for 27, ...,
     * 'ba' for 52 etc. Longer string are also available (e.g. 'aaa' for 702 or 'cyva' for 70200). For arguments greater than 25
     * recursive call is used
     * @param number number that is to be changed into string
     * @return proper string
     */
    private String getStringFromNumber(int number) {
        if (number / 26 == 0) //for numbers from 0 to 26 return single letter ('a' to 'z')
            return String.valueOf(Character.toChars(number+97));
        else //for greater numbers return longer string
            return getStringFromNumber(number / 26 - 1) + getStringFromNumber(number % 26);
    }

    /**
     * Method creating list of subsets (so called powerset)
     * @param originalSet the set that is the source of elements
     * @return list of subsets (including entire set and empty set)
     */
    private List<SortedSet<NumberedString>> powerSet(SortedSet<NumberedString> originalSet) {
        List<SortedSet<NumberedString>> subsets = new LinkedList<>();
        if (originalSet.isEmpty()) {
            subsets.add(new TreeSet<>());
            return subsets;
        }
        List<NumberedString> list = new ArrayList<>(originalSet);
        NumberedString head = list.get(0);
        SortedSet<NumberedString> rest = new TreeSet<>(list.subList(1, list.size()));
        for (SortedSet<NumberedString> set : powerSet(rest)) {
            SortedSet<NumberedString> newSet = new TreeSet<>();
            newSet.add(head);
            newSet.addAll(set);
            subsets.add(newSet);
            subsets.add(set);
        }
        subsets.sort((o1, o2) -> o2.size() - o1.size());
        /*subsets.sort(new Comparator<SortedSet<NumberedString>>() {
            @Override
            public int compare(SortedSet<NumberedString> o1, SortedSet<NumberedString> o2) {
                return o2.size() - o1.size();
            }
        });*/
        return subsets;
    }

    /**
     * Additional method wrapping powerSet() method. This method removes an empty set
     * @param originalSet the set that is the source of elements
     * @return list of subsets without an empty set
     */
    private List<SortedSet<NumberedString>> createNotEmptySubsets(SortedSet<NumberedString> originalSet) {
        List<SortedSet<NumberedString>> allSubsets = powerSet(originalSet);
        allSubsets.removeIf(SortedSet<NumberedString>::isEmpty);
        //equivalent statements:
        //allSubsets.removeIf(numberedStrings -> numberedStrings.isEmpty());
        /*allSubsets.removeIf(new Predicate<SortedSet<NumberedString>>() {
            @Override
            public boolean test(SortedSet<NumberedString> numberedStrings) {
                return numberedStrings.isEmpty();
            }
        });*/
        return allSubsets;
    }

    /**
     * Method generating list of subdomains
     * @param attr current attribute which domain is to be splited
     * @param ratio user-defined ratio, it gets the value of MIN_RULES_PER_SCHEMA parameter
     * @param attrsAmount amount of attributes which are concerned in the current schema
     * @return
     */
    private List<SortedSet<NumberedString>> makeSubdomains (Attribute attr, int ratio, int attrsAmount) {
        SortedSet<NumberedString> domain = attr.getType().getNoWeightDomain();
        List<SortedSet<NumberedString>> toReturn = new LinkedList<>();
        Random rand = new Random();
        int i=0, end;
        int maxStep = (int) (domain.size()/Math.pow(ratio,(double)1/attrsAmount));
        //max step to wielkosc dziedziny / ilosc regul (dla 1 atrybutu);
        //wielkosc dziedziny / pierwiastek attrsAmount-stopnia z ilosci regul
        
        //ilosc wytworzonych poddziedzin (a w konsekwencji ilosc regul) jest zawsze wieksza niz parametr ratio.
        //ale przy testach ta roznica maleje wraz ze wzrostem parametru ratio.
        NumberedString [] arrayDomain = domain.toArray(new NumberedString[domain.size()]);

        if (attr.getXTTClass().equals(Attribute.CLASS_SIMPLE)) { //if the attribute is simple
            while (i < arrayDomain.length) {                    //just add disjoint subdomains in a loop
                end = i+1+rand.nextInt(maxStep);
                toReturn.add(new TreeSet<>());
                for (int j=i; j<end; j++)
                    toReturn.get(toReturn.size()-1).add(arrayDomain[(j<arrayDomain.length ? j : arrayDomain.length-1)]);
                i=end;
            }
        } else {                        //if the attribute is general
            while (i < arrayDomain.length) {
                end = i+1+rand.nextInt(maxStep);
                List<NumberedString> domainList = new LinkedList<>(attr.getType().getNoWeightDomain());
                domainList = domainList.subList(i, (end+1 <= domainList.size() ? end+1 : domainList.size()));
                //(up) split domain into disjoint subdomains
                //(down) and add all nonempty subsets to returned list
                toReturn.addAll(createNotEmptySubsets(new TreeSet<>(domainList)));
                i = end;
            }
        }
        return  toReturn;
    }

    public String getModelSummary() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nCreated types: ").append(types.size());
        sb.append("\nCreated attributes: ").append(attributes.size());
        sb.append("\nCreated schemes: ").append(schemas.size());
        sb.append("\nCreated rules: ").append(rules.size());
        return sb.toString();
    }
}
