package xttgen;

import exceptions.BuilderException;
import exceptions.NotInTheDomainException;
import java.util.LinkedList;

/**
 * Class representing single schema in HMR.
 * @author Piotr Misiak
 */
public final class Schema {
    /**
    * This field contains schema name. This should be the default identifier that
    * schemas are referred.
    */
    protected String name;
    /**
     * List of input attributes in the schema.
     */
    protected LinkedList<Attribute> inputAttrs;
    /**
     * List of output attributes in the schema.
     */
    protected LinkedList<Attribute> outputAttrs;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedList<Attribute> getInputAttrs() {
        return inputAttrs;
    }

    public void setInputAttrs(LinkedList<Attribute> inputAttrs) {
        this.inputAttrs = inputAttrs;
    }

    public LinkedList<Attribute> getOutputAttrs() {
        return outputAttrs;
    }

    public void setOutputAttrs(LinkedList<Attribute> outputAttrs) {
        this.outputAttrs = outputAttrs;
    }
    
    private Schema(Schema.Builder builder) throws NotInTheDomainException{		
        this.setName(builder.name);
        this.setInputAttrs(builder.inputAttrs);
        this.setOutputAttrs(builder.outputAttrs);
    }
    
    /**
     * Overriden toString() method returns the complete definition of the schema 
     * as it's defined by the HMR language.
     * @return string representation of the schema
     */
    @Override
    public String toString() {
        return "\r\nxschm " + this.name + " :\r\n " + this.inputAttrs + " ==> " + this.outputAttrs + ".";
    }

    /**
     * Nested class used for creating new Schema object.
     */
    static class Builder {
        private String name;
        private LinkedList<Attribute> inputAttrs;
        private LinkedList<Attribute> outputAttrs;
        /**
         * Method creating new Schema object from the current existing Schema.Builder object.
         * @return new Schema object basen on the current Schema.Builder object
         * @throws BuilderException if at least one of required fields is not set
         * @throws NotInTheDomainException when a problem in Schema constructor occurs
         */
        public Schema build() throws BuilderException, NotInTheDomainException {
            if(name == null || inputAttrs == null || outputAttrs == null) {
                throw new BuilderException("Error while building Schema ("+name+"). Name, input attributes or output attribute"
                        + " was not defined.");
            }else {
                return new Schema(this);
            }
        }
        public Builder setName(String name) {
            this.name = name;
            return this;
        }
        public Builder setInputAttrs(LinkedList<Attribute> inputAttrs) {
            this.inputAttrs = inputAttrs;
            return this;
        }

        public Builder setOutputAttrs(LinkedList<Attribute> outputAttrs) {
            this.outputAttrs = outputAttrs;
            return this;
        }
    }
    
}
