package xttgen;

import java.io.*;
import java.util.*;

/**
 * The main class of the XTTGen V.4.0 application.
 *
 * @author Piotr Misiak
 * @version 4.0
 */
public class XTTGen {
    /**
     * The array containing names of the arguments which are used while the application works.
     * It's used to check if all necessary arguments are correctly defined in the config file.
     * It is important to change this array if some new arguments will be used in newer version.
     */
    public static final String[] ARGS_NAMES = {
            "TYPES_AMOUNT", "BASES_RATIO", "DOMAIN_SIZE", "IS_ORDERED", "STEP",
            "ATTRIBUTES_AMOUNT", "SCHEMAS_AMOUNT", "MIN_ATTRS_AMOUNT", "MAX_ATTRS_AMOUNT",
            "SIZE_LOWER_BOUND", "SIZE_UPPER_BOUND", "MIN_RULES_PER_SCHEMA"};

    /**
     * Main method
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Error. Too few argumenst.");
            System.exit(-1);
        }
        BufferedReader inputStream;
        Locale.setDefault(Locale.US);
        Map<String, String> am = new HashMap<>(); //arguments map
        File file = new File(args[0]);
        System.out.println("> Processing the " + file.getAbsolutePath() + " config file ...");
        try {
            inputStream = new BufferedReader(new FileReader(args[0]));
            String line;
            while ((line = inputStream.readLine()) != null) {
                if (line.startsWith("#define")) {
                    String[] lineArray = line.split(" ");
                    am.put(lineArray[1], lineArray[2].trim());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean correctArgs = true;
        for (String arg : ARGS_NAMES) {
            if (am.get(arg) == null) {
                System.err.println("Error! The argument " + arg + " is missing or improperly defined.");
                correctArgs = false;
            }
        }
        if (!correctArgs) {
            System.err.println("The application will halt now.");
            System.exit(-1);
        }
        System.out.println("Processing the config file finished.");
        //------------------------------------------------------------------------------
        Generator g = new Generator();
        g.createModel(Integer.parseInt(am.get("TYPES_AMOUNT")),
                am.get("BASES_RATIO"),
                Integer.parseInt(am.get("DOMAIN_SIZE")),
                am.get("IS_ORDERED").equalsIgnoreCase("yes"),
                Double.parseDouble(am.get("STEP")));
        g.createAttributes(Integer.parseInt(am.get("ATTRIBUTES_AMOUNT")));
        g.createSchemas(Integer.parseInt(am.get("SCHEMAS_AMOUNT")),
                Integer.parseInt(am.get("MIN_ATTRS_AMOUNT")),
                Integer.parseInt(am.get("MAX_ATTRS_AMOUNT")));
        g.createRules(Integer.parseInt(am.get("SIZE_LOWER_BOUND")),
                Integer.parseInt(am.get("SIZE_UPPER_BOUND")),
                Integer.parseInt(am.get("MIN_RULES_PER_SCHEMA")));
        g.createStates();
        System.gc();
        //------------------------------------------------------------------------------
        BufferedWriter outputStream;
        try {
            file = new File(args[1]);
            System.out.println("\n> Saving data to the file "+file.getAbsolutePath());
            outputStream = new BufferedWriter(new FileWriter(args[1]));
            g.saveModel(outputStream);
            g.saveAttributes(outputStream);
            g.saveSchemas(outputStream);
            g.saveRules(outputStream);
            g.saveStates(outputStream);
            outputStream.close();
            System.out.println("Data were successfully saved to the file.");
            System.out.println("-----------------------");
            System.out.println("Model summary: "+g.getModelSummary());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
