/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xttgen;

import assist.Formulae;
import exceptions.BuilderException;
import java.util.LinkedList;

/**
 * Class representing single rule in HMR.
 * @author Piotr Misiak
 */
public final class Rule {

    /**
     * Field containing the name of the rule.
     */
    private String name;
    /**
     * Field containing the reference to the owning Schema object.
     */
    private Schema schema;
    /**
     * Field containing the list of input conditions for the rule.
     */
    private LinkedList<Formulae> conditions;

    /**
     * Field containing the list of output results of the rule (executed when conditions are satisfied).
     */
    private LinkedList<Formulae> results;

    public void setName (String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    /**
     * Method returning the conditions list.
     * @return the condotions list of the rule.
     */
    public LinkedList<Formulae> getConditions() {
        return conditions;
    }

    public void setConditions(LinkedList<Formulae> conditions) {
        this.conditions = conditions;
    }
    
    /**
     * Method returning the results list.
     * @return the results list of the rule.
     */
    public LinkedList<Formulae> getResults() {
        return results;
    }

    public void setResults(LinkedList<Formulae> results) {
        this.results = results;
    }

    public Schema getSchema() {
        return schema;
    }

    public void setSchema(Schema schema) {
        this.schema = schema;
    }
    
    /**
     * Overriden toString() method returns the complete definition of the rules,
     * as it's defined by the HMR language.
     * @return complete string representation of the rule
     */
    @Override
    public String toString() {
        return "\r\nxrule " + schema.getName() + "/" + name + " :\r\n" + conditions + "\r\n==>\r\n" + results + ".\r\n";
    }    
    
    /**
     * Private parametric constructor used in the nested class while building new Rule object.
     * @param builder the source Builder object
     */
    private Rule(Rule.Builder builder) {		
        this.setName(builder.name);
        this.setSchema(builder.schema);
        this.setConditions(builder.conditions);
        this.setResults(builder.results);
    }
    
    static class Builder {
        private String name;
        private Schema schema;
        private LinkedList<Formulae> conditions;
        private LinkedList<Formulae> results;

        public Builder() { }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSchema(Schema schema) {
            this.schema = schema;
            return this;
        }

        public Builder setConditions(LinkedList<Formulae> conditions) {
            this.conditions = conditions;
            return this;
        }

        public Builder setResults(LinkedList<Formulae> results) {
            this.results = results;
            return this;
        }
        
        public Rule build() throws BuilderException {
            if(name == null || schema == null || conditions == null || results == null)
                throw new BuilderException("Error while building a rule. At least one of the fields is not defined.");
            else
                return new Rule(this);
        }
    }
}
